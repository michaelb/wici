# WICI-CI

WICI-CI is a tiny Python 2 app that provides for a very simple CI test
runner. It's only ~150 lines of code.  WICI.py-CI stands for WICI Continuous
Integration, which in turn stands for, "Wheel Inventing Continuous Integration"
Continuous Integration. Technically, it's best to call it "WICI-CI Continous
Integration Service", or WICI-CI CI for short.

It's not intended for large or serious projects, for that use something real,
like Jenkins, but for adding CI to simple, personal FOSS projects, it can do in
a pinch. Plus, the code is very tiny, so adding or changing stuff is easy.


# Installation

0 dependencies, other than Python2! Just download the file and run it.

If you want to serve the build status information to the public internet, you
might consider putting it behind a proxy such as nginx, it's likely fragile to
high traffic.

# Configuration

## Server conf

Configuring wici.py is done via env variables. The only essential env variable
is a whitelist of bitbucket user accounts. So, for me, I run wici.py as follows:

    WICI_WHITELIST="michaelb" ./wici.py localhost:9988

CLI arguments are the same as web.py, so port number can be a single positional
argument if you wish.

To Deploy: Copy or clone the wici.py file, along with `success.png` and
`failure.png` to some location on a publicly facing server. Then, run it. I'd
recommend running it in screen to easily detach it.

## Hook conf

The only repo conf absolutely necesary is setting up a hook. In Bitbucket, this
can be done in `Admin -> Settings -> Integrations -> Hooks`. Create a POST hook
as follows:

    http://yourdomainname.com:8090/hook/bitbucket/username/reponame/

Where "yourdomainname.com" is the server or IP of your CI server, and 8090  is
the port, and username is your account name or team name, and reponame is the
name of the repo.

Now, whenever you push changes, it will pull the default branch and run the
script `./tests/run.sh`. The nicest thing is you don't have to touch the server
again, just get it running once and it will server any repos with a username in
the whitelist!

## Repo conf - adding more steps

Optionally, you can give custom repo configuration. To do this, just put create
`.wici.ini` file in the top level dir of your repo. Full options are below,
with their defaults:

    [run]
    tests=./tests/run.sh

    [log]
    status=%s.status.log

    [image]
    success=success.png
    failure=failure.png

Run is where you add steps. If you add more than one step it will be executed
from top to bottom. If one step fails (that is, gives a non-0 exit code), it
will stop executing any more steps. This can be used, for example, a build or
deploy from going through if your tests fail.


# Results

The status of the build server is available at:

    /status/bitbucket/username/reponame/

For a simple graphical status, such as for embedding on a site, go to:

    /statuspng/bitbucket/username/reponame/

# Server support

By default, both Bitbucket and Github should work (I guess). If you want to use
a custom repo location, specify that in `WICI_CUSTOM`, and then use the path
/hook/custom/ for your POST post-commit hook.

