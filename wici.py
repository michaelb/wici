#!/usr/bin/env python
import os
import re
import time
import sys
import BaseHTTPServer
import traceback
from subprocess import check_output, CalledProcessError, STDOUT
from ConfigParser import RawConfigParser
from ConfigParser import Error as ConfigError

######################## CONFIG
cfg = lambda name, default: os.environ.get("WICI_" + name, default)
USER_WHITELIST = cfg("WHITELIST", "").split(",")
PATH = cfg('PATH', "/tmp/wici/")
RATELIMIT = int(cfg('RATELIMIT', "5")) # at least 5 seconds between reqs
CONF_NAME = cfg('CONF_NAME', ".wici.ini")
CUSTOM = cfg('CUSTOM', None)
SERVE_STATUS = cfg("SERVE_STATUS", "true").lower() == "true"
dirname = os.path.abspath(os.path.dirname(__file__))
SERVER_WHITELIST = {
    'bitbucket': "http://bitbucket.org/%s/%s.git",
    'github': "http://github.com/%s/%s.git",
}

######################## RATE LIMIT
last_request = 0.0
def ratelimit():
    global last_request
    assert time.time() - last_request > RATELIMIT
    last_request = time.time()

######################## CONF OBJECT
class Conf(object):
    DEFAULT = {
        'log': { 'status': '%s.status.log' },
        'image': {
            'success': os.path.join(dirname, 'success.png'),
            'failure': os.path.join(dirname, 'failure.png'),
        },
    }

    def __init__(self, git_uri, repo_path):
        self.git_uri = git_uri
        self.repo_path = repo_path
        self.parser = RawConfigParser()
        ini_path = os.path.join(repo_path, CONF_NAME)
        if os.path.exists(ini_path):
            self.parser.read([ini_path])

        status = self.get('log', 'status')
        self.status_path = status % repo_path.rstrip(os.path.sep)

    def get_run(self):
        try: items = self.parser.items("run")
        except ConfigError: items = []
        return items or [('tests', 'tests/run.sh')]

    def get(self, section, name):
        try:
            return self.parser.get(section, name)
        except ConfigError:
            return self.DEFAULT.get(section, {}).get(name)

######################## ROUTING
def parse_path(path):
    path = path.strip('/').split('/')
    action = path.pop(0)
    if path == ['custom']:
        return action, CUSTOM, CUSTOM.split('/')[-1]
    host, user, repo_name = path
    assert user in USER_WHITELIST and host in SERVER_WHITELIST
    git_uri = SERVER_WHITELIST[host] % (user, repo_name)
    return action, git_uri, repo_name

######################## ROUTES
def do_statuspng(conf):
    # Serve up PNG
    is_success = open(conf.status_path).readline().startswith('success')
    png_path = conf.get('image', 'success' if is_success else 'failure')
    return "image/png", open(png_path, 'rb').read()

do_status = lambda(conf): ("text/plain", open(conf.status_path).read())

def do_hook(conf):
    def run(cmd): check_output(cmd, stderr=STDOUT, shell=True)
    ratelimit()
    if not os.path.exists(conf.repo_path): # do initial checkout
        os.makedirs(conf.repo_path)
        run("git clone %s %s" % (conf.git_uri, conf.repo_path))
        conf = Conf(conf.git_uri, conf.repo_path) # re-load
    os.chdir(conf.repo_path) # change into checked out dir
    run("git pull %s" % conf.git_uri) # pull any updates
    output = ''
    success = True
    for name, command in conf.get_run(): # run all tests
        try: # Attempt to do action
            out = check_output(command, stderr=STDOUT, shell=True)
        except CalledProcessError as e:
            out = e.output
            success = False
        output += '------------- Step [%s] %s:\n%s' % (name, command, out)
        if not success: break

    output = "%s:\n\n%s" % ("success" if success else "failure", output)
    open(conf.status_path, "w+").write(output)
    return "text/plain", "success" if success else "failure"

ROUTES = {"hook": do_hook, }
if SERVE_STATUS:
    ROUTES.update({"status": do_status, "statuspng": do_statuspng })

######################## SERVER
class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_POST(s): s.do_GET()
    def do_GET(s):
        try:
            assert re.match('^[a-zA-Z0-9/\.-]$', s.path) # prevent shenanigans
            action, git_uri, repo_name = parse_path(s.path)
            repo_path = os.path.join(PATH, repo_name)
            conf = Conf(git_uri, repo_path)
            header, content = ROUTES[action](conf)
            s.send_response(200)
        except Exception as e:
            header, content = "text/plain", '404 i dont liek u'
            s.send_response(404)
            sys.stderr.write("[%s] 500 ERROR: %s\n%s" %
                    (time.asctime(), repr(e), traceback.format_exc()))
        s.send_header("Content-type", header)
        s.end_headers()
        s.wfile.write(content)

######################## MAIN
def main():
    if len(sys.argv) == 2 and sys.argv[1] == 'test':
        print "Simulated tests"
        sys.exit(0)

    if (len(USER_WHITELIST) < 1 or not USER_WHITELIST[0]) and (not CUSTOM):
        sys.stderr.write("ERROR: Need to specify env parameter WICI_WHITELIST \n"
                         "as a comma separated list whitelist of users for \n"
                         "github and bitbucket.\n")
        sys.exit(1)

    host = sys.argv[1].partition(":") if len(sys.argv) > 1 else ("0.0.0.0", "9988")
    httpd = BaseHTTPServer.HTTPServer((host[0], int(host[1])), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % host
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    main()

